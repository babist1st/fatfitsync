#! /usr/bin/python3

from .sync.fatconnect import FatConnect
from .sync.fitconnect import FitConnect
import datetime

class SyncModule():
    def __init__(self, day = None, **kwargs):
        super().__init__()
        self.setup_connections()
        self.create_obj()
        self.day = day

    def setup_connections(self):
        pass

    def create_obj(self):
        pass

    def update_day(self):
        pass

    def notify_msg(self, msg='None', *args):
        print(msg,args)

    def progress(self, percent):
        print(percent)

    def connection_status(self,*args):
        self.con_check = []
        self.con_collect_check()
        self.con_status()

    def loop_connections(func):
         def loop(self, *args):
             for con in self.connections.values():
                func(self, con)
         return loop

    @loop_connections
    def con_collect_check(self, con):
        self.con_check.append(con.connected)

    @loop_connections
    def con_status(self, con):
        #if len(self.connection.values()) >= len(con_check)
        #    self.con_check.append(con.connected)
        if any(self.con_check) is False:
            self.connected = False
        if all(self.con_check) is True:
            self.connected = True


class Fatfit(SyncModule):
    def __init__(self):
        super().__init__()

    def setup_connections(self):
        self.connections = {
            "fs":Fatsync(),
            "fit":Fitsync()
            }

    def create_obj(self):
        self.fs = self.connections['fs']
        self.fit = self.connections['fit']

    def get_day_data(self):
        self.d = self.fs.get_data(self.day)
        if not self.d ['calories']:
            self.notify_msg("FS: Haven't registered data yet")
            self.fitdata=''
            return
        self.fitdata = self.fit.get_day_data(self.day)

    def delete_day_data(self):
        try:
            logid = self.fitdata['foods'][0]['logId']
            self.notify_msg("Deleting data on", self.day.date())
            self.fit.delete_day_data(logid)
        except:
            self.notify_msg("No data on", self.day.date())
            return

    def add_day_data(self):
        data={
                    "foodName": "Fatsecret Daily Summary",
                    "mealTypeId":7,
                    "unitId":1,
                    "amount":1.00,
                    "date":str(self.day),
                    "brandName":"fatfitsync",
                    "calories":int(self.d['calories']),
                    "protein":self.d['protein'],
                    "totalCarbohydrate":self.d['carbohydrate'],
                    "totalFat":self.d['fat'],
                    "dietaryFiber":self.d['fiber'],
                    "sodium":self.d['sodium']
                }
        # print("Updating data")
        self.notify_msg("Updating data")
        self.fit.f.foods_log(date=self.day, data=data)
        for k, d in data.items():
            self.notify_msg(str(k),str(d))

    def update(self):
        self.progress(0)
        self.get_day_data()
        # self.percent = 0
        # time.sleep(3)
        self.progress(30)
        # self.percent = 30
        # time.sleep(3)
        self.delete_day_data()
        self.progress(60)
        # self.percent = 60
        # time.sleep(3)
        self.add_day_data()
        self.progress(100)
        # self.percent = 100

        self.notify_msg(msg="Done {}".format(self.day.date()))
        self.running = False

    def main(self):
        dt = datetime.datetime
        ddt = datetime.timedelta(days = -1)

        today = dt.today()
        yesterday = today + ddt

        self.day = today
        print(day.date())

if __name__ == "__main__":
    f = Fatfit()
    f.main()
    f.update()
